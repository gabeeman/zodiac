package edu.svsu.csis.zodiac;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class SignCategoryActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign2);

        ListView listSigns = getListView();
        ArrayAdapter<Sign> listAdapter = new ArrayAdapter<Sign>(
                this,
                android.R.layout.simple_list_item_1,
                Sign.signs);
        listSigns.setAdapter(listAdapter);
    }

    @Override
    public void onListItemClick(ListView listView,
                                View itemView,
                                int position,
                                long id){
        Intent intent = new Intent(SignCategoryActivity.this, SignActivity.class);
        intent.putExtra(SignActivity.EXTRA_SIGNNO, (int) id);
        startActivity(intent);
    }

}
