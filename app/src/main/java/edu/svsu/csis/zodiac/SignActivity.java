package edu.svsu.csis.zodiac;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class SignActivity extends Activity {

    public static final String EXTRA_SIGNNO = "signNo";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign2);


        int signNo = (int)getIntent().getExtras().get(EXTRA_SIGNNO);
        Sign sign = Sign.signs[signNo];

        ImageView photo = (ImageView)findViewById(R.id.photo);
        photo.setImageResource(sign.getImageResourceId());
        photo.setContentDescription(sign.getName());

        TextView name = (TextView)findViewById(R.id.name);
        name.setText(sign.getName());

        TextView description = (TextView)findViewById(R.id.description);
        description.setText(sign.getDescription());

        TextView symbol = (TextView)findViewById(R.id.symbol);
        symbol.setText(sign.getSymbol());

        TextView month = (TextView)findViewById(R.id.month);
        month.setText(sign.getMonth());

    }


}
